defmodule Bowex.Utils do
  def only_alphanumeric(string) do
    Stream.unfold(string, &String.next_codepoint/1)
    |> Stream.filter(&alphanumeric?/1)
    |> Enum.reduce("", &(&2<>&1))
  end

  def split_into_words(string) do
    String.split(string, ~r{\s|\t|\n}, trim: true)
  end

  @doc """
  Checks whether the given parameter is an alphanumeric character.

  ## Examples
      iex> Bowex.Utils.alphanumeric?("b")
      true
      iex> Bowex.Utils.alphanumeric?("O")
      true
      iex> Bowex.Utils.alphanumeric?(5)
      false
  """
  def alphanumeric?(char) when byte_size(char) == 1 and char >= "a" and char <= "z", do: true
  def alphanumeric?(char) when byte_size(char) == 1 and char >= "A" and char <= "Z", do: true
  def alphanumeric?(char) when byte_size(char) == 1 and char >= "0" and char <= "9", do: true
  def alphanumeric?(_), do: false

  def hidden_file?(path) do
    String.split(path, "/")
    |> Enum.reverse
    |> hd
    |> String.starts_with?([".", "_"])
  end
end