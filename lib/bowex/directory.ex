defmodule Bowex.Directory do
  @behaviour Bowex.BowParser
  import Bowex.Utils, only: [hidden_file?: 1]

  @pretty_print_opts [width: 80]
  
  @doc """
  Parses bag of words for each file present in the directory and subdirectories
  recursively.
  """
  def parse(path), do: parse(path, recursive: true)

  def parse(path, recursive: true) do
    files = get_files(path)

    files
    |> Enum.map(fn file_path -> Task.async(Bowex.File, :parse, [file_path]) end)
    |> Enum.map(fn task -> Task.await(task) end)
    |> Enum.zip(files)
    |> Enum.map(fn {a, b} -> %{file: b, bow: a} end)
  end

  @doc """
  Creates a bag of words corresponding to all the files within the given
  directory.
  """
  def bow(path) do
    parse(path)
    |> Enum.flat_map(fn %{bow: bow} -> bow end)
    |> Enum.uniq
  end

  @doc """
  Prints bag of words for all files in the given directory to an output file
  """
  def print_for(dir, output_file_path) do
    {:ok, output_file} = File.open(output_file_path, [:read, :write])

    parse(dir)
    |> Enum.each(fn file_result -> IO.inspect(output_file, file_result, @pretty_print_opts) end)

    File.close(output_file)
  end

  @doc """
  Returns list of children files inside a given folder with their
  full relative path.
  """
  defp get_child_files(path) do
    case File.ls(path) do
      {:ok, files} -> Enum.flat_map(files, &(get_files(path <> "/" <> &1)))
      {:error, reason} -> IO.puts "ls failed on #{path} for reason: #{reason}"
      _ -> IO.puts "Unhandled case in parsing directory"
    end
  end

  defp get_files(path), do: get_files(path, hidden: hidden_file?(path))

  defp get_files(_path, hidden: true), do: []
  defp get_files( path, hidden: false) do
    case File.stat(path) do
      {:ok, %File.Stat{type: :directory}} -> get_child_files(path)
      {:ok, %File.Stat{type: :regular}} -> [path]
      _ -> []
    end
  end
end