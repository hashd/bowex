defmodule Bowex.BowParser do
  use Behaviour

  defcallback parse(String.t) :: [String.t | Map.t]
  defcallback bow(String.t) :: [String.t | Map.t]
end