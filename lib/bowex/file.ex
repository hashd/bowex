defmodule Bowex.File do
  @behaviour Bowex.BowParser
  
  def parse(path_to_file) do
    import Bowex.Utils, only: [only_alphanumeric: 1, split_into_words: 1]
    IO.puts "Parsing BOW for #{path_to_file}"
    
    File.stream!(path_to_file, [:read])
      |> Stream.map(&String.downcase/1)
      |> Stream.flat_map(&split_into_words/1)
      |> Stream.map(&only_alphanumeric/1)
      |> Stream.filter(&(&1!=nil && &1!=""))
      |> Enum.uniq
  end

  def bow(path_to_file), do: parse(path_to_file)
end