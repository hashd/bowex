defmodule Bowex.Mixfile do
  use Mix.Project

  def project do
    [app: :bowex,
     version: "0.0.1",
     elixir: "~> 1.0",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps,
     description: description]
  end

  def application do
    [applications: [:logger],
     mod: {Bowex, []}]
  end

  defp deps do
    []
  end

  defp description do
    """
    Experiments with Elixir to process bag of words
    """
  end
end
